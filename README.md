### docker command
* config
```
sudo APP_HOST_PHP56_PORT=8156 APP_HOST_PHP73_PORT=8173 DB_HOST_PORT=38100 /usr/local/bin/docker-compose \
--env-file=./docker/env/local.compose.env \
-f ./docker/docker-compose-php73.yml \
-f ./docker/docker-compose-php56.yml \
-f ./docker/docker-compose-mysql.yml \
-f ./docker/docker-compose-network.yml \
config
```

* up --build -d
```
sudo APP_HOST_PHP56_PORT=8156 APP_HOST_PHP73_PORT=8173 DB_HOST_PORT=38100 /usr/local/bin/docker-compose \
--env-file=./docker/env/local.compose.env \
-f ./docker/docker-compose-php73.yml \
-f ./docker/docker-compose-php56.yml \
-f ./docker/docker-compose-mysql.yml \
-f ./docker/docker-compose-network.yml \
up --build -d
```

* down --rmi all --volumes
```
sudo APP_HOST_PHP56_PORT=8156 APP_HOST_PHP73_PORT=8173 DB_HOST_PORT=38100 /usr/local/bin/docker-compose \
--env-file=./docker/env/local.compose.env \
-f ./docker/docker-compose-php73.yml \
-f ./docker/docker-compose-php56.yml \
-f ./docker/docker-compose-mysql.yml \
-f ./docker/docker-compose-network.yml \
down --rmi all --volumes
```

* attach
```
sudo docker exec -it lamp73.moekyun.me.local.http /bin/bash
sudo docker exec -it lamp73.moekyun.me.local.mysql /bin/bash
```

* 一覧
```
sudo docker volume ls
sudo docker network ls
```

* mysql
```
mysql -uroot -p -hlamp73.moekyun.me.local.mysql
```
---
